import Enemy from './Enemy';

export default class SpikeM extends Enemy {

    constructor(config) { 
        super({
            scene: config.scene,
            x: config.x,
            y: config.y,
            key: config.key
        });

        config.scene.physics.world.enable(this);
        config.scene.add.existing(this);

        this.scene = config.scene;
        this.player = this.scene.player;

        this.body.setImmovable(true);

        this.b_up = false;

        this.n_timeout = this.scene.time.now;

        this.b_up = true;

    }

    create() {
        this.scene.physics.add.collider(this, this.scene.backgroundLayer);
    }

    update() {

        this.scene.physics.world.overlap(this, this.player, () => {
            if (this.b_up) {
                this.kill_event();
            }
        });
        
        this.show_hide_spike();



    }

    show_hide_spike() {
        if (this.n_timeout < this.scene.time.now) {
            this.n_timeout = this.scene.time.now + 2000;
            if (this.b_up) {
                this.hide_spike();
            } else {
                this.show_spike();
            }
        }
    }

    show_spike() {
        this.anims.play("anim_show_spike", true);
        this.b_up = true;
    }

    hide_spike() {
        this.anims.play("anim_hide_spike", true);
        this.b_up = false;
    }
}