import Enemy from './Enemy.js';

export default class EnemyBigM extends Enemy {

    constructor(config) {
        // x + 32 porque mide 64 px de ancho
        super({
            scene: config.scene,
            x: config.x + 32,
            y: config.y - 16,
            key: config.key
        });
        // Le ponemos la anchura que corresponde
        // this.body.width = 64;
        // Seteamos el offset a 0, 0 para que no se descuadre la imagn
        this.body.setOffset(0, 0);
        this.body.setVelocity(0, 0).setBounce(0,0).setCollideWorldBounds(false);
        
    }

    update(time, delta) {
        // Colisiones con el mundo
        this.scene.physics.world.collide(this, this.scene.backgroundLayer);

        // Colisiones con los stop objects
        this.scene.physics.world.collide(this, this.scene.stopsGroup, () => {
            // Colision con paredes u objetos
            if (this.body.velocity.x === 0) {
                this.direction = -this.direction;
                this.body.velocity.x = this.direction;
                // Rotación del sprite
                this.toggleFlipX();
                
            }    
        });

        this.scene.physics.world.collide(this, this.player, () => {
            this.kill_event();
        });

        this.body.velocity.x = this.direction;

        
        

        
    }
    
}
