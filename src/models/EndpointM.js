import LocalStorage from './LocalStorageM';

export default class EndpointM extends Phaser.GameObjects.Sprite {
    constructor(config) {
        super(config.scene, config.x, config.y, config.key);
        config.scene.physics.world.enable(this);
        config.scene.add.existing(this);

        this.scene = config.scene;

        // instanciamos y esperamos
        this.body.setImmovable(true);
        this.body.setFriction(1,1);
        // this.body.moves = false;
        this.body.setAllowGravity(false);
        
        // Referencia al jugador
        this.player = this.scene.player;
        this.coinsGroup = this.scene.coinsGroup;

        this.body.setSize(32, 96);
        this.alpha = 0;
        this.body.checkCollision.up = false;
        this.body.checkCollision.down = false;

        this.current_level = this.scene.current_level;
        
    }

    create() {
        if (!this.scene) return;
        this.scene.physics.add.collider(this, this.player, () => {
            const n_total_coins = this.scene.n_total_coins;
            
            if (this.player.n_collected_coins === n_total_coins) {
                if (this.scene.b_restarting) return;
                this.scene.b_restarting = true;
                
                this.scene.b_restarting = true;
                this.scene.physics.pause();
                this.player.alpha = 0;
                this.scene.backgroundLayer.alpha = 0;
                
                
                LocalStorage.set_element("n_last_text_index", -1);
                LocalStorage.set_element("n_level", parseInt(this.scene.current_level) + 1);
                
                if (parseInt(this.scene.current_level) >= 17) {
                    LocalStorage.remove_element("n_level");
                    this.scene.current_level = 0;
                    this.scene.scene.start("MenuScene");
                    return;
                }
                gtag('level', parseInt(this.scene.current_level));
                this.scene.scene.start("MainScene", {level: parseInt(this.scene.current_level) + 1});
                
            }
        });
    }

    update(time, delta) {}
}