export default class Enemy extends Phaser.GameObjects.Sprite {

    constructor(config) {
        super(config.scene, config.x, config.y, config.key);

        config.scene.physics.world.enable(this);
        config.scene.add.existing(this);

        this.scene = config.scene;

        // instanciamos y esperamos
        this.body.setVelocity(0, 0).setBounce(0, 0).setCollideWorldBounds(false);
        this.body.allowGravity = true;

        // Referencia al jugador
        this.player = this.scene.player;

        // Referencias a los stops object
        this.scene.a_stops = this.scene.stopsGroup;

        // Velocidad y dirección por defecto
        this.direction = 80;

        // this.body.setSize(32, 25);
        
    }

    kill_event() {
        
        
        this.player.player_dead();
        // this.scene.physics.pause();
        // this.player.alpha = 0;
            
        this.scene.time.addEvent({
            delay: 1800,
            callback: () => this.scene.scene.restart()
        });
        
        
    }
    
}