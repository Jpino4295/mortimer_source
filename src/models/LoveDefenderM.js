export default class LoveDefenderM extends Phaser.GameObjects.Sprite {
    
    constructor(config) {
        super(config.scene, config.x + 16, config.y - 16, config.key);
        
        config.scene.physics.world.enable(this);
        config.scene.add.existing(this);

        this.scene = config.scene;
        this.player = this.scene.player;
        this.body.allowGravity = false;
        this.setDepth(-1);
        this.body.setImmovable(true);
        this.scaleX = 2;
        this.scaleY = this.scaleX;
        
    }

    create() {
        this.scene.physics.add.collider(this, this.scene.backgroundLayer);

        this.anims.load('anim_ld_idle');
        this.anims.play('anim_ld_idle');
    }

    update(time, delta) {
        
        // his.scene.physics.world.collide(o_coin, this.player, this.get_coins);
        // this.scene.physics.world.overlap(this, this.player, () => {
        //     this.player.n_collected_coins++;
        //     this.scene.updateCoinsText();

        //     this.destroy();
        // });

    }

}