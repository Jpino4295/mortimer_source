// https://phaser.io/examples/v3/view/physics/arcade/tween-body
import Platform from './Platform.js';

export default class PlatformXM extends Platform {
    constructor(config) {
        super({
            scene: config.scene,
            x: config.x + Math.floor(config.width/2),
            y: config.y - Math.floor(config.height/2),
            key: config.key,
            width: config.width,
            height: config.height
        });
        this.config = config;
        this.direction = 140;
        this.body.setBounce(0, 0);
        
        
    }

    update(time, delta) {

        this.scene.physics.world.collide(this, this.player, () => {
            
            this.player.can_jump = true;
            this.player.n_jumps = 2;
            this.player.on_floor = true;
            
        });

        this.scene.physics.world.collide(this, this.scene.a_stops, () => {
            this.direction = -this.direction;
            this.body.velocity.y = this.direction;

            // Rotación del sprite
            // this.toggleFlipY(); 
        });

        this.body.setVelocityY(this.direction);
        this.body.setBounce(1);
        
    }
}