import 'phaser';

import { MainScene } from './scenes/MainScene';
import { MenuScene } from './scenes/MenuScene';
import UIPlugin from '../templates/ui/ui-plugin.js';

const gameConfig = {
    width: 700,
    height: 800,
    pixelArt: true,
    antialiasing: false,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 1500 },
            // debug: true
        }
    },
    scene: [MenuScene, MainScene],
    // scene: MainScene,
    plugins: {
        scene: [{
            key: 'rexUI',
            plugin: UIPlugin,
            mapping: 'rexUI'
        }]
    }
};

new Phaser.Game(gameConfig);