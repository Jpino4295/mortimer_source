//https://gamedevacademy.org/how-to-make-a-mario-style-platformer-with-phaser-3/
// https://github.com/nkholski/phaser3-es6-webpack/blob/master/src/scenes/GameScene.js

// https://itnext.io/modular-game-worlds-in-phaser-3-tilemaps-3-procedural-dungeon-3bc19b841cd

// Cameras: https://github.com/photonstorm/phaser/issues/3738

import PlayerM from '../models/PlayerM';
import StopsM from '../models/StopsM';
import TextsM from '../models/TextsM';
import CoinsM from '../models/CoinsM';
import PlatformXM from '../models/PlatformXM';
import PlatformYM from '../models/PlatformYM';
import RemovablePlatformM from '../models/RemovablePlatformM';
import EndpointM from '../models/EndpointM';
import EnemySimpleM from '../models/Enemies/EnemySimpleM';
import EnemyBigM from '../models/Enemies/EnemyBigM';
import SpikeM from '../models/Enemies/SpikeM';
import FireSpitterM from '../models/Enemies/FireSpitterM';
import YJumperM from '../models/Enemies/YJumperM';
import XJumperM from '../models/Enemies/XJumperM';
import DeathM from '../models/DeathM';
import LocalStorage from '../models/LocalStorageM';
import LoveDefenderM from '../models/LoveDefenderM';

export class MainScene extends Phaser.Scene {

    constructor() {
        super("MainScene");
        
    }
    init(o_data) {
        this.b_restarting = false;

        if (o_data.level) {
            this.current_level = o_data.level;
            this.b_audio_playing = true;
        } else {
            let n_level = LocalStorage.get_element("n_level");
            
            if (n_level != null) {
                this.current_level = n_level;
            } else {
                this.current_level = 0;
            }
            
        }

        

        
        
        // this.current_level = 13;

        this.b_created = false;
        this.b_win = false;
        
    }

    preload(){
        // Load used assets
        this.load_objects_assets();
        
        // Bg tiles
        this.load.image('game_tiles', 'assets/sprites/blacknwhite.png');

        // Map from Tiled
        console.log("Recogiendo nivel ->", `assets/maps/lvl${this.current_level}.json`);
        
        this.load.tilemapTiledJSON(`lvl${this.current_level}`, `assets/maps/lvl${this.current_level}.json`);
        // this.load.tilemapTiledJSON(`lvl${this.current_level}`, `assets/maps/lvl16.json`);
        // this.load.tilemapTiledJSON(`lvl${this.current_level}`, `assets/maps/test_text.json`);

        
        
    }

    create() {

        this.bg_sound = this.bg_sound || this.sound.add("bg_sound");
        
        this.stop_audio();

        this.start_audio();
        
        // Collision tiles
        this.floor_tiles = [1, 2, 3, 9, 10, 11, 17, 18, 19, 22, 25, 26, 27, 28, 33, 34];

        this.load_map();
        
        this.create_player();

        this.load_physics_system();

        this.create_cursors();

        this.setCamera();

        this.create_animations();

        // Create all element groups
        this.enemyGroup = this.add.group();

        this.stopsGroup = this.add.group();

        this.coinsGroup = this.add.group();

        this.platformsGroup = this.add.group();

        this.textsGroup = this.add.group();

        this.deathGroup = this.add.group();
        
        this.ldGroup = this.add.group();

        this.parseObjectLayers();

        this.n_total_coins = this.coinsGroup.children.entries.length;

        this.n_collected_coins = `${this.player.n_collected_coins}/${this.n_total_coins}`;

        this.n_texts_seen = 0;

        this.createBottomTable();

        if (this.platformsGroup.children.entries.length > 0) {
            this.platformsGroup.children.entries.forEach(
                (sprite) => {
                    sprite.create();
                }
            );
        }
        
        if (this.coinsGroup.children.entries.length > 0) {
            this.coinsGroup.children.entries.forEach(
                (sprite) => {
                    sprite.create();
                }
            );
        }

        if (this.enemyGroup.children.entries.length > 0) {
            this.enemyGroup.children.entries.forEach(
                (sprite) => {
                    sprite.create();
                }
            );
        }

        if (this.deathGroup.children.entries.length > 0) {
            this.deathGroup.children.entries.forEach(
                (sprite) => {
                    sprite.create();
                }
            );
        }
        
        if (this.endpointObject != undefined) {
            this.endpointObject.create();
        }

        if (this.ldGroup.children.entries.length > 0) {
            this.ldGroup.children.entries.forEach(
                (sprite) => {
                    sprite.create();
                }
            );
        }
        
        this.physics.world.TILE_BIAS = 32;

        this.b_created = true;

        
    }

    update(time, delta) {

        if (!this.b_created) return;

        if (this.player != undefined) {
            this.load_controls();
        }
        
        // Limitar velocidad de caida para solucionar problema con las colisiones
        if (this.player != undefined && this.player.body.velocity.y > 800){
            this.player.body.setVelocityY(800);
        }

        // // Check if the player is on the floor to modify the jump variables
        this.player.is_on_floor(this.player);

        this.player.update(time, delta);

        // // Update from every sprites
        if (this.textsGroup) {
            this.textsGroup.children.entries.forEach(
                (sprite) => {
                    sprite.update(time, delta);
                }
            );
        }

        

        if (this.enemyGroup) {
            this.enemyGroup.children.entries.forEach(
                (sprite) => {
                    sprite.update(time, delta);
                }
            );
        }
        
        if (this.platformsGroup) {
            this.platformsGroup.children.entries.forEach(
                (sprite) => {
                    sprite.update(time, delta);
                }
            );
        }

        if (this.coinsGroup) {
            this.coinsGroup.children.entries.forEach(
                (sprite) => {
                    sprite.update(time, delta);
                }
            );
        }

        if (this.endpointObject) {
            this.endpointObject.update(time, delta);
        }

        // Check win state only if player haven't collected the coins yet
        if (!this.b_win) {            
            this.check_win_state();
        }
        
    }

    load_map() {
        // TODO: Hay que tener en cuenta los mapas verticales largos de poner 4 tiles de más para que se vea bien el marcador

        // Load tilemap
        this.map = this.add.tilemap(`lvl${this.current_level}`);
        // Set tilemap image
        var tileset = this.map.addTilesetImage('blacknwhite','game_tiles');
        // Create bg layer
        this.backgroundLayer = this.map.createDynamicLayer('Bg', tileset);
        // Collisions
        this.map.setCollision(this.floor_tiles, true, true, this.backgroundLayer);

        
        
    }

    load_objects_assets() {
        // Player
        this.load.spritesheet('player', 'assets/sprites/player/player.png', { frameWidth: 23, frameHeight: 43 });
        this.load.spritesheet('player_walk', 'assets/sprites/player/walk.png', { frameWidth: 23, frameHeight: 43 });
        this.load.spritesheet('player_idle', 'assets/sprites/player/idle.png', { frameWidth: 23, frameHeight: 43 });
        this.load.spritesheet('player_on_wall', 'assets/sprites/player/on_wall.png', { frameWidth: 24, frameHeight: 39 });
        this.load.spritesheet('player_on_wall_anim', 'assets/sprites/player/wall_animation.png', { frameWidth: 24, frameHeight: 39 });
        this.load.spritesheet('player_skull_idle', 'assets/sprites/player/skull_idle.png', { frameWidth: 23, frameHeight: 23 });
        this.load.spritesheet('player_skull_walk', 'assets/sprites/player/skull_walk.png', { frameWidth: 23, frameHeight: 23 });
        this.load.spritesheet('player_jump', 'assets/sprites/player/player_jump.png', { frameWidth: 23, frameHeight: 37 });
        this.load.spritesheet('player_talking', "assets/sprites/player/player_talking.png", { frameWidth: 23, frameHeight: 58 });
        this.load.spritesheet('player_head', "assets/sprites/player/player_head.png", { frameWidth: 86, frameHeight: 77 });
        this.load.spritesheet('brother_head', "assets/sprites/player/brother_head.png", { frameWidth: 86, frameHeight: 77 });
        this.load.spritesheet('player_dead', 'assets/sprites/player/player_dead.png', { frameWidth: 23, frameHeight: 24 });

        // // Coins
        this.load.spritesheet('coin', 'assets/sprites/coin_sprite.png', { frameWidth: 32, frameHeight: 32 });

        // Environment
        this.load.spritesheet('single_platform', 'assets/sprites/environment/single_platform.png', {frameWidth: 64, frameHeight: 32});
        this.load.spritesheet('double_platform', 'assets/sprites/environment/double_platform.png', {frameWidth: 96, frameHeight: 32});
        
        // // Enemies
        this.load.spritesheet('enemy_simple', 'assets/sprites/enemies/enemy_simple.png', { frameWidth: 32, frameHeight: 15 });
        this.load.spritesheet('show_spike_anim', 'assets/sprites/enemies/show_spikes.png', { frameWidth: 32, frameHeight: 32 });
        this.load.spritesheet('hide_spike_anim', 'assets/sprites/enemies/hide_spikes.png', { frameWidth: 32, frameHeight: 32 });
        this.load.spritesheet('fire_spitter', 'assets/sprites/enemies/fire_spitter.png', { frameWidth: 32, frameHeight: 32 });
        this.load.spritesheet('bullet', 'assets/sprites/enemies/bullet.png', { frameWidth: 20, frameHeight: 16 });
        this.load.spritesheet('y_jumper', 'assets/sprites/enemies/y_jumper.png', { frameWidth: 20, frameHeight: 32 });
        this.load.spritesheet('x_jumper', 'assets/sprites/enemies/x_jumper.png', { frameWidth: 32, frameHeight: 20 });

        // Death
        this.load.spritesheet('death', 'assets/sprites/death/ghost-idle.png', { frameWidth: 64, frameHeight: 80 });
        this.load.spritesheet('death_head', 'assets/sprites/death/death_head.png', { frameWidth: 75, frameHeight: 54 });

        // Love defender
        this.load.spritesheet('love_defender', 'assets/sprites/love_defender/idle.png', {frameWidth: 32, frameHeight: 33});

        // Font
        this.load.bitmapFont("8bit", 'assets/fonts/8bit.png', 'assets/fonts/8bit.fnt');

        
        
        this.load.audio("bg_sound", ['assets/audio/bg_sound.mp3']);
        this.load.audio("dialog_sound", ["assets/audio/dialog_sound.mp3"]);
        
        
    }

    load_physics_system() {
        this.physics.world.setBounds(0, 0, this.map.widthInPixels, this.map.heightInPixels);
        this.physics.add.collider(this.player, this.backgroundLayer);
        
    }

    create_player() {
        this.player = new PlayerM({
            scene: this,
            x: 64,
            y: this.map.heightInPixels - 64,
            key: "player"
        });
    }

    create_cursors() {
        this.cursors = this.input.keyboard.createCursorKeys();

    }

    create_animations() {
        this.anims.create({
            key: 'anim_coin',
            frames: this.anims.generateFrameNumbers('coin'),
            frameRate: 12,
            yoyo: 6,
            repeat: -1
        });

        this.anims.create({
            key: 'anim_death',
            frames: this.anims.generateFrameNumbers('death'),
            frameRate: 2,
            // yoyo: 6,
            repeat: -1
        });

        this.anims.create({
            key: 'anim_enemy_simple',
            frames: this.anims.generateFrameNumbers('enemy_simple'),
            frameRate: 12, 
            yoyo: 6,
            repeat: -1
        });

        this.anims.create({
            key: 'anim_show_spike',
            frames: this.anims.generateFrameNumbers('show_spike_anim'),
            frameRate: 24,
            repeat: 0
        });

        this.anims.create({
            key: 'anim_hide_spike',
            frames: this.anims.generateFrameNumbers('hide_spike_anim'),
            frameRate: 24,
            repeat: 0
        });

        this.anims.create({
            key: 'anim_player_walk',
            frames: this.anims.generateFrameNumbers('player_walk'),
            frameRate: 4, 
            yoyo: 6,
            repeat: -1
        });

        this.anims.create({
            key: 'anim_player_talking',
            frames: this.anims.generateFrameNumbers('player_talking'),
            frameRate: 4, 
            // yoyo: 6,
            repeat: -1
        });

        this.anims.create({
            key: 'anim_player_idle',
            frames: this.anims.generateFrameNumbers('player_idle'),
            frameRate: 2, 
            yoyo: 6,
            repeat: -1
        });

        this.anims.create({
            key: 'anim_ld_idle',
            frames: this.anims.generateFrameNumbers('love_defender'),
            frameRate: 2, 
            yoyo: 6,
            repeat: -1
        });

        this.anims.create({
            key: 'anim_player_skull_idle',
            frames: this.anims.generateFrameNumbers('player_skull_idle'),
            frameRate: 2, 
            yoyo: 6,
            repeat: -1
        });

        this.anims.create({
            key: 'anim_player_skull_walk',
            frames: this.anims.generateFrameNumbers('player_skull_walk'),
            frameRate: 16, 
            yoyo: 6,
            repeat: -1
        });

        this.anims.create({
            key: 'anim_player_dead',
            frames: this.anims.generateFrameNumbers('player_dead'),
            frameRate: 3, 
            // yoyo: 6,
            // repeat: 1
        });

        this.anims.create({
            key: 'anim_on_wall',
            frames: this.anims.generateFrameNumbers('player_on_wall_anim'),
            frameRate: 4, 
            yoyo: 6,
            repeat: 0
        });

    }

    load_controls() {
        this.player.set_controlls();
    }

    setCamera() {
        
        
        this.cameras.main.setBounds(0,0, this.map.widthInPixels, this.map.heightInPixels);
        this.cameras.main.startFollow(this.player, false, 0.1, 0.1, 0, 0);
        

        // Black bg, avoiding overload the map
        this.cameras.main.setBackgroundColor("#000");
    }
    
    /**
     * This checks everytime for the layer, in case there isn't a specific object layer in the map
     */
    parseObjectLayers() {

        if (this.map.getObjectLayer('player') != null) {
            this.map.getObjectLayer('player').objects.forEach((o_player) => {
                this.player.x = o_player.x;
                this.player.y = o_player.y;
            });
        }
        

        if (this.map.getObjectLayer('stops') != null) {
            this.map.getObjectLayer('stops').objects.forEach((o_stop) => {
                let stopObject;
                stopObject = new StopsM({
                    scene: this,
                    key: 'stops',
                    x: o_stop.x,
                    y: o_stop.y
                });
    
                this.stopsGroup.add(stopObject);
            });
        }

        if (this.map.getObjectLayer('texts') != null) {
            this.map.getObjectLayer('texts').objects.forEach((o_text) => {
                let textObject;
                textObject = new TextsM({
                    scene: this,
                    key: 'text',
                    x: o_text.x,
                    y: o_text.y,
                    text: o_text.properties.text,
                    index: this.map.getObjectLayer('texts').objects.indexOf(o_text)
                });
    
                this.textsGroup.add(textObject);
            });
        }
        

        if (this.map.getObjectLayer('coins') != null) {
            this.map.getObjectLayer('coins').objects.forEach((o_coin) => {
                let coinObject;
                coinObject = new CoinsM({
                    scene: this,
                    key: 'coin',
                    x: o_coin.x,
                    y: o_coin.y
                });
    
                this.coinsGroup.add(coinObject);
            });
        
            // Lleva la cuenta. Usado para el endpoint
            this.n_total_coins = this.coinsGroup.children.entries.length;
        }

        if (this.map.getObjectLayer('platforms') != null) {
            this.map.getObjectLayer('platforms').objects.forEach((o_platform) => {
                let platformObject;

                let s_key = 'single_platform';
                if (o_platform.properties.width > 64) {
                    s_key = 'double_platform';
                }
                
                switch (o_platform.name) {
                    case "platform_y":
                        platformObject = new PlatformYM({
                            scene: this,
                            key: s_key,
                            x: o_platform.x,
                            y: o_platform.y,
                            width: o_platform.properties.width,
                            height: o_platform.properties.height,
                        });
                        break;
                    case "platform_x":
                        platformObject = new PlatformXM({
                            scene: this,
                            key: s_key,
                            x: o_platform.x,
                            y: o_platform.y,
                            width: o_platform.properties.width,
                            height: o_platform.properties.height,
                        });
                        break;
                    case "platform_remove":
                        platformObject = new RemovablePlatformM({
                            scene: this,
                            key: s_key,
                            x: o_platform.x,
                            y: o_platform.y,
                            width: o_platform.properties.width,
                            height: o_platform.properties.height
                        });
                        break;
                }
                
                
                
                this.platformsGroup.add(platformObject);
            
                
            });
        }
        

        if (this.map.getObjectLayer('enemies_simple') != null) {
            this.map.getObjectLayer('enemies_simple').objects.forEach((enemy) => {
                let enemyObject;
                enemyObject = new EnemySimpleM({
                    scene: this,
                    key: 'enemy_simple',
                    x: enemy.x,
                    y: enemy.y
                });
                
                this.enemyGroup.add(enemyObject);
            });
        }

        if (this.map.getObjectLayer('spikes') != null) {
            this.map.getObjectLayer('spikes').objects.forEach((spike) => {
                let spikeObject;
                spikeObject = new SpikeM({
                    scene: this,
                    key: 'spikes',
                    x: spike.x,
                    y: spike.y
                });
                
                this.enemyGroup.add(spikeObject);
            });
        }

        if (this.map.getObjectLayer('fire_spitter') != null) {
            this.map.getObjectLayer('fire_spitter').objects.forEach((fire_spitter) => {
                let fire_spitterObject;
                fire_spitterObject = new FireSpitterM({
                    scene: this,
                    key: 'fire_spitter',
                    x: fire_spitter.x,
                    y: fire_spitter.y,
                    aim_direction: fire_spitter.properties.aim
                });
                
                this.enemyGroup.add(fire_spitterObject);
            });
        }

        if (this.map.getObjectLayer('y_jumper') != null) {
            this.map.getObjectLayer('y_jumper').objects.forEach((y_jumper) => {
                let y_jumperObject;
                y_jumperObject = new YJumperM({
                    scene: this,
                    key: 'y_jumper',
                    x: y_jumper.x,
                    y: y_jumper.y,
                    tiles: parseInt(y_jumper.properties.tiles_move),
                    b_bot: y_jumper.properties.b_bot == "true" ? true : false,
                    b_top: y_jumper.properties.b_top == "true" ? true : false,

                });
                
                this.enemyGroup.add(y_jumperObject);
            });
        }

        if (this.map.getObjectLayer('x_jumper') != null) {
            this.map.getObjectLayer('x_jumper').objects.forEach((x_jumper) => {
                let x_jumperObject;
                x_jumperObject = new XJumperM({
                    scene: this,
                    key: 'x_jumper',
                    x: x_jumper.x,
                    y: x_jumper.y,
                    tiles: parseInt(x_jumper.properties.tiles_move),
                    b_left: x_jumper.properties.b_left == "true" ? true : false,
                    b_right: x_jumper.properties.b_right == "true" ? true : false,

                });
                
                this.enemyGroup.add(x_jumperObject);
            });
        }

        
        
        if (this.map.getObjectLayer('enemies_big')) {
            this.map.getObjectLayer('enemies_big').objects.forEach((enemy) => {
                let enemyObject;
                enemyObject = new EnemyBigM({
                    scene: this,
                    key: 'enemies_big',
                    x: enemy.x,
                    y: enemy.y
                });
                this.enemyGroup.add(enemyObject);
            });
        }

        
        if (this.map.getObjectLayer("endpoint") != null) {
            this.map.getObjectLayer("endpoint").objects.forEach((o_endpoint) => {
                this.endpointObject = new EndpointM({
                    scene: this,
                    x: o_endpoint.x+48,
                    y: o_endpoint.y+16,
                    key: null
                });
                
            });
        }

        if (this.map.getObjectLayer('death') != null) {
            this.map.getObjectLayer('death').objects.forEach((o_death) => {
                let deathObject;
                deathObject = new DeathM({
                    scene: this,
                    key: 'death',
                    x: o_death.x,
                    y: o_death.y
                });
                
                this.deathGroup.add(deathObject);
            });
        }

        if (this.map.getObjectLayer('love_defender') != null) {
            this.map.getObjectLayer('love_defender').objects.forEach((o_ld) => {
                let ldObject;
                ldObject = new LoveDefenderM({
                    scene: this,
                    key: 'love_defender',
                    x: o_ld.x,
                    y: o_ld.y
                });
                
                this.ldGroup.add(ldObject);
            });
        }
        
    }

    check_win_state() {
        if (this.coinsGroup.children.entries.length == 0) {
            this.b_win = true;
            
            this.map.filterTiles((tile) => {
                if (tile.index == 22 || tile.index == 13) {
                    let n_x = tile.x;
                    let n_y = tile.y;

                    let n_tile = 21;

                    if (tile.index == 13) {
                        n_tile = 15;
                    }

                    this.map.putTileAt(n_tile, n_x, n_y);
                }
            });
        }
    }

    createBottomTable() {

        
        let rect = this.add.rectangle(350, 750, 700, 100, 0x000000).setScrollFactor(0).setStrokeStyle(2, 0xffffff);

        let left_rect = this.add.rectangle(350/3, 750, 700/3, 100, 0x000000).setScrollFactor(0).setStrokeStyle(2, 0xffffff);

        let right_rect = this.add.rectangle(700-(350/3 + 350/3), 750, 700/3 + 700/3 , 100, 0x000000).setScrollFactor(0).setStrokeStyle(2, 0xffffff);
        
        let icon = this.add.image(40, 745, "coin", 0).setScrollFactor(0);
        this.coinText = this.add.bitmapText(70, 737, "8bit", this.n_collected_coins, 24).setScrollFactor(0);


        this.talker_icon = this.add.image(275, 745, "coin", 0).setScrollFactor(0).setVisible(false);
        this.talker_text = this.add.bitmapText(325, 737, "8bit", "",  24).setScrollFactor(0).setVisible(false);
        this.talker_next_icon = this.add.bitmapText(550, 785, "8bit", "X or space to continue",  12).setScrollFactor(0).setVisible(false);
        

    }

    updateCoinsText() {
        let s_text = `${this.player.n_collected_coins}/${this.n_total_coins}`;
        this.coinText.setText(s_text);
    }

    start_audio() {
        this.b_audio_playing = true;
        
        this.bg_sound.loop = true;
        this.bg_sound.play();
        
    }

    stop_audio() {
        this.bg_sound.stop();
    }
    
}