


export default class PlayerM extends Phaser.GameObjects.Sprite{

    constructor(config) {

        super(config.scene, config.x, config.y, config.key);

        config.scene.physics.world.enable(this);
        config.scene.add.existing(this);

        this.body.setBounce(0).setCollideWorldBounds(true);
        
        // Variables generales player
        this.player_speed = 200;
        this.player_jump_speed = 600;
        this.in_duck = false;
        this.n_jumps = 2;
        this.n_collected_coins = 0;
        this.player_height = this.body.height;
        this.player_width = this.body.width * 0.5;

        this.original_width = this.body.width;

        this.body.width = this.player_width;
        this.body.setOffset(this.player_width/2,  0);

        // Variables de control de salto
        this.can_jump = true;
        this.on_wall = false;
        this.second_jump = false;
        this.on_floor = true;

        this.n_facing = 1;

        this.b_paused = false;

    }

    update(time, delta) {
        this.on_wall = false;

        if (this.b_paused) {
            this.body.setVelocity(0);
            this.toggle_walk_animation(false);
        }

        // Levantamos el player si no tiene nada encima
        if (this.scene.cursors.down.isUp && this.in_duck) {
            this.player_up();
        }
        

        // Bugfix: De la otra manera, cuando detectaba que estaba en el techo tambien contaba como trepando
        // Anterior: if (!this.on_floor && this.body.checkWorldBounds()) {
        if ((this.body.blocked.left || this.body.blocked.right) && !this.on_floor) {
                
                this.on_wall = true;
                this.n_jumps = 1;
                this.player_up();
                
                this.body.setVelocityY(0);
                
                // Subir y bajar
                this.wall_controls();
            // }
            
        }

        if (!this.on_floor && !this.on_wall) {
            
            if (!this.in_duck) {
                this.setTexture("player_jump");
            } else {
                this.setTexture("player_skull_idle", 0);
            }
        }
        
    }

    set_controlls() {
        if (!this.b_paused) {
            if (this.scene.cursors.left.isDown) {
                this.set_facing(true);
    
            
                this.toggle_walk_animation(true);
                this.set_velocity_x(false);
                
            } else if (this.scene.cursors.right.isDown) {
                this.set_facing(false);
    
                this.toggle_walk_animation(true);
    
                this.set_velocity_x(true);
                
            } else {
                this.body.setVelocityX(0);
                this.toggle_walk_animation(false);
                
            }
    
            // Jump
            this.scene.input.keyboard.on('keydown_SPACE', this.handle_jump, this);
            this.scene.input.keyboard.on('keydown_X', this.handle_jump, this);
    
            // Duck
            this.scene.input.keyboard.on('keydown_DOWN', this.handle_duck, this);
            this.scene.input.keyboard.on('keyup_DOWN', this.player_up, this);    
        }
    }

    wall_controls() {
        
        if (this.scene.cursors.up.isDown && this.on_wall) {
            this.anims.play("anim_on_wall", true);
            this.body.setVelocityY(-this.player_speed);
        }
        //  else if (this.scene.cursors.down.isDown && this.on_wall) {
        //     this.anims.play("anim_on_wall", true);
        //     this.body.setVelocityY(this.player_jump_speed * 0.7);
        // }
         else {
            this.setTexture("player_on_wall");
        }

    }
    
    toggle_walk_animation(b_walk) {
        if (!this.b_paused) {
            if (!this.in_duck && this.on_floor) {
                this.body.height = this.player_height;
                if (b_walk) {
                    this.anims.play("anim_player_walk", true);
                } else {
                    this.anims.play("anim_player_idle", true);
                }
                
            } else if (this.in_duck && this.on_floor) {
                if (b_walk) {
                    this.anims.play("anim_player_skull_walk", true);
                } else {
                    this.anims.play("anim_player_skull_idle", true);
                }
            }
        }
        
    }

    toggle_dialog_animation(b_walk) {
        if (!this.in_duck && this.on_floor) {
            this.player_up();
            this.body.height = 58;
            this.anims.play("anim_player_talking", true);
        }
    }

    handle_duck() {
        if (!this.b_paused) {
            if (!this.in_duck && !this.on_wall) {
                this.in_duck = true;
                this.body.height = this.player_height / 2;
                // this.y = this.y - this.body.height/2;
                this.body.width = this.original_width;
                this.body.setOffset(0);
    
                
                // this.setTexture("main_character");
                
            }
        }
        
    }

    handle_jump() {
        if (!this.b_paused) {
            if (this.can_jump || this.on_wall){
            
                this.on_floor = false;
                if (!this.in_duck) {
                    this.body.setVelocityY(-this.player_jump_speed);
                } else {
                    this.body.setVelocityY(-this.player_jump_speed / 1.5);
                }
                
                this.n_jumps--;
                
                // if (this.n_jumps <= 0) {
                //     this.second_jump = true;
                //     this.can_jump = false;
                // }
    
                this.can_jump = false;
    
                if (this.on_wall) {
                    this.body.setVelocityX(this.body.velocity.x * -3);
                }
    
                this.on_wall = false;
            }
        }
        
    }

    player_up() {
        
        if (this.in_duck && (!this.is_upper_tile())) {
            this.in_duck = false;
            this.body.height = this.player_height;
            this.body.width = this.player_width;
            this.body.setOffset(this.player_width/2, 0);
            
            this.y = this.y - this.body.height / 2.2;

            this.setFrame(0);
        }
    }

    check_if_up() {
        if (this.cursors.down.isUp && this.in_duck && ((this.upper_tile === null) || (this.tile_floor.indexOf(this.upper_tile.index) !== -1))) {
            this.player_up();
          }
    }

    is_on_floor(player) {
        if (player.body.blocked.down) {
            player.on_floor = true;
            player.can_jump = true;
            player.on_wall = false;
            player.n_jumps = 2;
        }

    }

    is_upper_tile() {
      return( this.scene.backgroundLayer.hasTileAtWorldXY(this.body.position.x, this.body.position.y - this.body.height) && this.scene.backgroundLayer.hasTileAtWorldXY(this.body.position.x + this.body.width, this.body.position.y - 32));
    }

    set_facing(b_facing) {
        // Right
        if (!b_facing) {
            this.setFlipX(false);
        } else if (b_facing) {
            this.setFlipX(true);
        }
    }

    set_velocity_x(b_positive) {
        let n_velocity = b_positive ? this.player_speed : -this.player_speed;
        if (!this.in_duck) {
            this.body.setVelocityX(n_velocity);
            if (!this.on_floor) {
                this.body.setVelocityX(n_velocity * 1.5);
            }
        } else {
            this.body.setVelocityX(n_velocity * 1.7);
            
            if (!this.on_floor) {
                this.body.setVelocityX(n_velocity * 4);
            }
        }
    }

    pause_player() {
        this.on_floor = true;
        this.player_up();
        this.b_paused = true;
        this.toggle_dialog_animation();
    }

    start_player() {
        this.toggle_walk_animation();
        this.b_paused = false;
    }

    player_dead() {
        this.on_floor = true;
        this.b_paused = true;
        this.body.allowGravity = false;
        this.body.setImmovable(true);
        this.anims.play("anim_player_dead", true);
    }
    
}