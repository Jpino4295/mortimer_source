export default class Platform extends Phaser.GameObjects.Sprite {
    constructor(config) {
        super(config.scene, config.x, config.y, config.key);
        config.scene.physics.world.enable(this);
        config.scene.add.existing(this);

        // Referencias a los stops object
        this.scene.a_stops = this.scene.stopsGroup;

        // instanciamos y esperamos
        this.body.setBounce(1, 1);
        this.body.setImmovable(true);
        this.body.setFriction(1,1);
        // this.body.moves = false;
        this.body.setAllowGravity(false);
        
        // Referencia al jugador
        this.player = this.scene.player;

        this.body.setSize(config.width, config.heihgt);
        
    }

    create() {}

    update(time, delta) {}

}