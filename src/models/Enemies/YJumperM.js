import Enemy from './Enemy';

export default class YJumperM extends Enemy {
    
    constructor(config) {
        super({
            scene: config.scene,
            x: config.x + 16,
            y: config.y - 16,
            key: config.key
        });

        this.body.setImmovable(true);
        this.body.allowGravity = false;

        // this.setAlpha(0);

        this.x_coordinate = config.x + 16;

        this.b_in_top = config.b_top;
        this.b_in_bottom = config.b_bot;

        this.to = this.y - 16 + config.tiles * 32;
        this.from = config.y - 16;

        // Fixme: Hotfix for starting bottom elements
        if (this.b_in_bottom) {
            this.toggleFlipY();
            this.from = this.y + 16 + config.tiles * 32;
            this.to = config.y - 16;
        }

    }

    create() { }


    update(time, delta) {

        if (this.b_in_top == true && this.body.velocity.y != 0) {
            if (this.y >= this.to) {
                // this.setAlpha(0);
                this.toggleFlipY();
                this.body.velocity.y = 0;
                this.b_in_top = false;
                this.b_in_bottom = true;
            }
        }

        if (this.b_in_bottom == true && this.body.velocity.y != 0) {
            if (this.y <= this.from) {
                // this.setAlpha(0);
                this.toggleFlipY();
                this.body.velocity.y = 0;
                this.b_in_top = true;
                this.b_in_bottom = false;
            }
        }        

        
        // Si está entre medio sprite antes y medio sprite despues, lanzamos el call
        if (Math.round(this.player.x) >= Math.round(this.x - 16) && Math.round(this.player.x) <= Math.round(this.x + 16)) {
            this.setAlpha(1);

            if (this.b_in_top) {
                this.scene.physics.moveTo(this, this.x_coordinate, this.to, 300);
            }

            if (this.b_in_bottom) {
                this.scene.physics.moveTo(this, this.x_coordinate, this.from, 300);
            }
               
        }

        this.scene.physics.add.overlap(this, this.player, () => {
            this.kill_event();
        });

    }




}