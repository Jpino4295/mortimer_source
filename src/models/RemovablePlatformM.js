import Platform from './Platform.js';

export default class PlatformXM extends Platform {
    constructor(config) {
        super({
            scene: config.scene,
            x: config.x + Math.floor(config.width/2),
            y: config.y - Math.floor(config.height/2),
            key: config.key,
            width: config.width,
            height: config.height
        });

        // this.direction = 140;

        // this.to = config.to;

        // this.body.setVelocityX(100);

        
    }

    update(time, delta) {
        
        // Colision con jugador
        this.scene.physics.world.collide(this, this.player, () => {
            
            
            setTimeout(() => {
                this.destroy();
              }, 1000);
              
            
            
            if (!this.body.touching.left || !this.body.touching.right) {
                this.player.can_jump = true;
                this.player.n_jumps = 2;
            }
            
        });
        
    }
}