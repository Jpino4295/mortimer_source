export default class LocalStorage {

    static set_element(s_key, s_value) {
        localStorage.setItem(s_key, s_value);
    }

    static get_element(s_key) {
        return localStorage.getItem(s_key);
    }

    static remove_element(s_key) {
        localStorage.removeItem(s_key);
    }

}