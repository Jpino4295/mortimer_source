import Enemy from './Enemy.js';

export default class EnemySimpleM extends Enemy {

    constructor(config) {
        super({
            scene: config.scene,
            x: config.x + 16,
            y: config.y - 16,
            key: config.key
        });
        this.body.setVelocity(0, 0).setBounce(0,0).setCollideWorldBounds(false);
        
    }

    create() {
        this.anims.load('anim_enemy_simple');
        this.anims.play('anim_enemy_simple');

        this.setScale(2);
    }

    update(time, delta) {
        // Colisiones con el mundo
        this.scene.physics.world.collide(this, this.scene.backgroundLayer);

        // Colisiones con los stop objects
        this.scene.physics.world.collide(this, this.scene.a_stops, () => {
            // Colision con paredes u objetos
            if (this.body.velocity.x === 0) {
                this.direction = -this.direction;
                this.body.velocity.x = this.direction;

                // Rotación del sprite
                this.toggleFlipX();
            }    
        });

        this.scene.physics.world.collide(this, this.player, () => {
            this.kill_event();
        });
        

        this.body.velocity.x = this.direction;

        
        

        
    }

    
    
}
