export default class StopM extends Phaser.GameObjects.Sprite  {

    constructor(config) {
        super(config.scene, config.x + 16, config.y - 16, config.key);
        config.scene.physics.world.enable(this);
        config.scene.add.existing(this);

        // instanciamos y esperamos
        this.body.setVelocity(0, 0).setBounce(1, 1).setCollideWorldBounds(false);
        this.body.allowGravity = false;
        this.body.setImmovable();
        this.alpha = 0;
        
    }

}