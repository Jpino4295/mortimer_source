// https://snowbillr.github.io/blog//2018-04-09-a-modern-web-development-setup-for-phaser-3/
const path = require("path");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');

module.exports = {
    entry: {
        app: './src/index.js'
    },
    
    output: {
        path: path.resolve(__dirname, 'www'),
        filename: 'app.bundle.js'
    },

    // Que pase por babel
    module: {
        rules: [
            {
                test: /\.js$/,
                include: path.resolve(__dirname, 'src/'),
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }
            }
        ]
    },

    plugins: [
        new CopyWebpackPlugin([
          {
            from: path.resolve(__dirname, 'index.html'),
            to: path.resolve(__dirname, 'www')
          },
          {
            from: path.resolve(__dirname, 'assets', '**', '*'),
            to: path.resolve(__dirname, 'www')
          }
        ]),

        new webpack.DefinePlugin({
            'typeof CANVAS_RENDERER': JSON.stringify(true),
            'typeof WEBGL_RENDERER': JSON.stringify(true),
            WEBGL_RENDERER: true, // I did this to make webpack work, but I'm not really sure it should always be true
            CANVAS_RENDERER: true // I did this to make webpack work, but I'm not really sure it should always be true
        }),

        new webpack.optimize.CommonsChunkPlugin({
            name: 'production-dependencies',
            filename: 'production-dependencies.bundle.js'
        })
    ],

    devServer: {
        contentBase: path.resolve(__dirname, 'www')
    }
};