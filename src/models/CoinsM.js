export default class CoinsM extends Phaser.GameObjects.Sprite {
    
    constructor(config) {
        super(config.scene, config.x + 16, config.y - 16, config.key);
        
        config.scene.physics.world.enable(this);
        config.scene.add.existing(this);

        this.scene = config.scene;

        // instanciamos y esperamos
        this.body.setVelocity(0, 0).setBounce(0, 0).setCollideWorldBounds(false);
        this.body.allowGravity = false;
        this.body.setImmovable();
        this.body.width = 32;
        this.body.height = 32;

        // Referencia al jugador para hacer las colisiones
        this.player = config.scene.player;

        
    }

    create() {
        this.anims.load('anim_coin');
        this.anims.play('anim_coin');
    }

    update(time, delta) {
        
        // his.scene.physics.world.collide(o_coin, this.player, this.get_coins);
        this.scene.physics.world.overlap(this, this.player, () => {
            this.player.n_collected_coins++;
            this.scene.updateCoinsText();

            this.destroy();
        });

    }

}