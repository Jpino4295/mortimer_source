import Platform from './Platform.js';

export default class PlatformXM extends Platform {
    constructor(config) {
        super({
            scene: config.scene,
            x: config.x + Math.floor(config.width/2),
            y: config.y - Math.floor(config.height/2),
            key: config.key,
            width: config.width,
            height: config.height
        });

        this.direction = 140;

        this.to = config.to;

        this.body.setVelocityX(100);

        
    }

    update(time, delta) {
        
        this.scene.physics.world.collide(this, this.player, () => {
            
            this.player.can_jump = true;
            this.player.n_jumps = 2;
            this.player.on_floor = true;
        
        });
        
        // Aqui utilizamos un collide y no un tween porque de esta manera el player se queda en la parte de arriba de la plataforma
        // Utilizamos el mismo sistema que para los enemigos
        this.scene.physics.world.collide(this, this.scene.a_stops, () => {
            this.direction = -this.direction;
            this.body.velocity.x = this.direction;

            // Rotación del sprite
            // this.toggleFlipX();
        });

        this.body.setVelocityX(this.direction);
        this.body.setBounce(1);


        

        
    }
}