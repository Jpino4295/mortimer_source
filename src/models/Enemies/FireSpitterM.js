import Enemy from './Enemy';

export default class FireSpitterM extends Enemy {

    constructor(config) { 
        super({
            scene: config.scene,
            x: config.x + 16,
            y: config.y - 16,
            key: config.key
        });
        

        config.scene.physics.world.enable(this);
        config.scene.add.existing(this);

        this.scene = config.scene;
        this.player = this.scene.player;

        this.body.setImmovable(true);
        this.body.allowGravity = false;

        this.n_timeout = this.scene.time.now;

        this.bullet = null;

        this.bulletsGroup = this.scene.physics.add.group({
            defaultKey: 'bullet',
            defaultFrame: 0,
            maxSize: 1
        });

        this.aim_direction = config.aim_direction;
        this.x_coordinate = this.x + 16;
        this.y_coordinate = this.y;
        

    }

    create() { 
        switch (this.aim_direction) {
            case "left": 
                this.toggleFlipX();
                this.x_coordinate = this.x - 16;
                break;
            case "top":
                this.angle = -90;
                this.x_coordinate = this.x;
                this.y_coordinate = this.y - 16;
                break;
            case "bottom":
                this.angle = 90;
                this.x_coordinate = this.x;
                this.y_coordinate = this.y + 16;
                break;
        }

    }

    update(time, delta) {
        this.shoot();

        
        this.scene.physics.world.collide(this.bulletsGroup,this.scene.backgroundLayer, (o_b) => {
            o_b.setActive(false);
            // o_b.setVisible(false);
            o_b.x = -100;
            o_b.y = -100;
        });
        

        this.scene.physics.world.overlap(this.bulletsGroup, this.scene.player, () => {
            this.kill_event();
        });

    }

    shoot() {
        if (this.n_timeout < this.scene.time.now) {
            this.n_timeout = this.scene.time.now + 1000;

            let bullet = this.bulletsGroup.get(this.x_coordinate, this.y_coordinate);
            if (bullet) {

                bullet.body.allowGravity = false;
                bullet.body.setImmovable(true);
    
                if (this.aim_direction == "right" || this.aim_direction == "left" ) {
                    let n_direction = this.aim_direction == "right" ? 1 : -1;
                    bullet.body.setVelocityX(300 * n_direction);
                } else if (this.aim_direction == "top" || this.aim_direction == "bottom") {
                    let n_direction = this.aim_direction == "bottom" ? 1 : -1;
                    bullet.body.setVelocityY(300 * n_direction);
                }

                switch (this.aim_direction) {
                    case "left": 
                    bullet.angle = 180;
                    break;
                case "top":
                    bullet.angle = -90;
                    break;
                case "bottom":
                    bullet.angle = 90;
                    break;
                }

            }
            
            
        }
    }
}