import PlayerM from "./PlayerM";
import LocalStorage from "./LocalStorageM";

export default class TextsM extends Phaser.GameObjects.Sprite  {

    constructor(config) {
        super(config.scene, config.x, config.y - 16, config.key);
        config.scene.physics.world.enable(this);
        config.scene.add.existing(this);

        this.scene = config.scene;
        this.o_text = config.text;

        // console.log(this.o_text);
        

        // instanciamos y esperamos
        this.body.setVelocity(0, 0).setBounce(0, 0).setCollideWorldBounds(false);
        this.body.allowGravity = false;
        this.body.height = 96;
        this.body.setImmovable();
        this.alpha = 0;
        this.b_showed = false;

        this.n_text_index = config.index;
        
        
    }

    update(time, delta) {
        this.scene.physics.world.overlap(this, this.scene.player, () => {

            let n_ls_text_index = LocalStorage.get_element("n_last_text_index");

            if (n_ls_text_index != null && this.n_text_index <= n_ls_text_index) {
                this.b_showed = true;
            }

            if (!this.b_showed) {
                LocalStorage.set_element("n_last_text_index", this.n_text_index);
                
                this.b_showed = true;
                

                this.scene.add.sprite();

                this.scene.n_texts_seen++;

                this.show_text(this.scene.talker_text, this.o_text.split(";"));
                
            }
            
            
            
        });
    }


    show_text(textbox, a_text, scene) {
        scene = scene || this.scene;
        
        if (a_text.length <= 0) {
            this.scene.talker_icon.setVisible(false);
            this.scene.talker_next_icon.setVisible(false);
            textbox.setVisible(false);
            // textbox.destroy();
        
            this.scene.player.start_player();
            return;
        }

        
        // console.log(a_text);

        let dialog_sound = this.scene.sound.add("dialog_sound");
        dialog_sound.play();
        
        let o_text = JSON.parse(a_text.shift());
        this.scene.player.pause_player();

        const s_text = o_text.text;

        if (o_text.who == "player") {
            // Icono jugador
            this.scene.talker_icon.setTexture("player_head", 0).setVisible(true);
            
        } else if (o_text.who == "brother") {
            // Icono hermano
            this.scene.talker_icon.setTexture("brother_head", 0).setVisible(true);
        } else if (o_text.who == "death") {
            this.scene.talker_icon.setTexture("death_head", 0).setVisible(true);
        } else if (o_text.who == "l_defender") {
            this.scene.talker_icon.setTexture("love_defender", 0).setVisible(true);
        }

        textbox.setVisible(true);
        this.scene.talker_next_icon.setVisible(true);
        textbox.setText(s_text);

        const o_x = scene.input.keyboard.addKey("X");
        const o_space = scene.input.keyboard.addKey("SPACE");

        o_x.once('up', () => {
            this.show_text(textbox, a_text, scene);
        });
        o_space.once('up', () => {
            this.show_text(textbox, a_text, scene);
        });
        
        // setTimeout(() => {
        //     this.show_text(textbox, a_text, scene);
        // }, 2000);
        

    }

}