import Enemy from './Enemy';

export default class XJumperM extends Enemy {
    
    constructor(config) {
        super({
            scene: config.scene,
            x: config.x + 16,
            y: config.y - 16,
            key: config.key
        });

        this.body.setImmovable(true);
        this.body.allowGravity = false;

        // this.setAlpha(0);

        this.to = this.x + config.tiles * 32;
        this.from = config.x + 16;

        this.b_in_left = config.b_left;
        this.b_in_right = config.b_right;

        // Fixme: Hotfix for starting bottom elements
        if (this.b_in_right) {
            this.toggleFlipX();
            this.from = this.x + config.tiles * 32;
            this.to = config.x + 16;
        }

    }

    create() {
        
     }


    update(time, delta) {

        if (this.b_in_left == true && this.body.velocity.x != 0) {
            if (this.x >= this.to) {
                // this.setAlpha(0);
                this.toggleFlipX();
                this.body.velocity.x = 0;
                this.b_in_left = false;
                this.b_in_right = true;
            }
        }

        if (this.b_in_right == true && this.body.velocity.x != 0) {
            if (this.x <= this.from) {
                // this.setAlpha(0);
                this.toggleFlipX();
                this.body.velocity.x = 0;
                this.b_in_left = true;
                this.b_in_right = false;
            }
        }        

        
        // Si está entre medio sprite antes y medio sprite despues, lanzamos el call
        if (Math.round(this.player.y) >= Math.round(this.y - 16) && Math.round(this.player.y) <= Math.round(this.y + 16)) {
            this.setAlpha(1);

            if (this.b_in_left) {
                this.scene.physics.moveTo(this, this.to, this.y, 300);
            }

            if (this.b_in_right) {
                this.scene.physics.moveTo(this, this.from, this.y, 300);
            }
               
        }

        this.scene.physics.add.overlap(this, this.player, () => {
            this.kill_event();
        });

    }




}