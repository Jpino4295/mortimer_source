import LocalStorage from '../models/LocalStorageM';

import MainScene from './MainScene';

export class MenuScene extends Phaser.Scene {

    constructor() {
        super("MenuScene");
        
    }

    preload() {
        this.load.bitmapFont("8bit", 'assets/fonts/8bit.png', 'assets/fonts/8bit.fnt');
        this.load.image('logo', 'assets/images/logo.png');
        this.load.spritesheet('menu_skull_idle', 'assets/sprites/player/skull_idle.png', { frameWidth: 23, frameHeight: 23 });
    }

    create() {

        this.add.image(350, 150, 'logo');

        let img_skull_play = this.add.image(240, 365, "menu_skull_idle", 0);
        img_skull_play.setVisible(false);

        const o_play_btn = this.add.bitmapText(270, 350, '8bit', 'Play', 48)
        .setInteractive()
        .on('pointerdown', this.on_play_clicked, this )
        .on('pointerover', () => {
            img_skull_play.setVisible(true);
        }, this)
        .on('pointerout', () => {
            img_skull_play.setVisible(false);
        }, this);

        if (LocalStorage.get_element("n_level") != null) {
            let img_skull_continue = this.add.image(210, 465, "menu_skull_idle", 0);
            img_skull_continue.setVisible(false);

            const o_continue_btn = this.add.bitmapText(230, 450, '8bit', 'Continue', 48)
            .setInteractive()
            .on('pointerdown', this.on_continue_clicked, this )
            .on('pointerover', () => {
                img_skull_continue.setVisible(true);
            }, this)
            .on('pointerout', () => {
                img_skull_continue.setVisible(false);
            }, this);;
        }

    }


    on_play_clicked() {
        LocalStorage.remove_element("n_level");
        LocalStorage.remove_element("n_last_text_index");
        this.scene.start("MainScene");
        
    }

    on_continue_clicked() {
        this.scene.start("MainScene");
    }

    on_hover() {
        console.log("Hover");
        
    }

}